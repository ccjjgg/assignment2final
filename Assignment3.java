import javax.swing.*;
import java.awt.*;
import java.applet.*;

public class Assignment3 extends JApplet {
	public void init() {
		String CorrectAdminUsername = "JustinTimberlake,PaulRyan,MittRomney,BarackObama,JoeBiden";
		String CorrectStudentUsername = "JustinCheng,SpikeLee,WoodyAllen,QuentinTarantino,MichaelBay";
		String CorrectStaffUsername = "MattDamon,JonahHill,JamesFranco,GeorgeClooney,LeoMessi";
		String CorrectAdminPassword = "Wade'931026";
		String CorrectStudentPassword = "931026'Wade";
		String CorrectStaffPassword = "Thanes'050999";
		String Iusername, Ipassword;
		String[] choices = { "Admin", "Student", "Staff" };
		String input = (String) JOptionPane.showInputDialog(null,
                                                            "Choose account type...", "Account Type",
                                                            JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
		if (input == "Admin") {
			Iusername = JOptionPane.showInputDialog("Enter Your Username");
			int index = CorrectAdminUsername.indexOf(Iusername);
			int count1 = 1, count2 = 1;
			while (index == -1) {
				Iusername = JOptionPane
                .showInputDialog("Username does not exist!Please Try Again.");
				index = CorrectAdminUsername.indexOf(Iusername);
				count1++;
				if ((count1 >= 3) && (index == -1)) {
					JOptionPane.showMessageDialog(null,
                                                  "System Failure, Please try again in 15 minutes",
                                                  "System Failure.", JOptionPane.ERROR_MESSAGE);
					break;
				}
			}
			if (index != -1) {
				Ipassword = JOptionPane.showInputDialog("Enter Your Password");
				while (count2 < 3) {
					if (Ipassword.equals(CorrectAdminPassword)) {
						JOptionPane.showMessageDialog(null, "Welcome Admin.",
                                                      "Welcome", JOptionPane.PLAIN_MESSAGE);
						break;
					} else {
						Ipassword = JOptionPane
                        .showInputDialog("Password incorrect!Please Try Again.");
						count2++;
					}
				}
				if (count2 >= 3) {
					JOptionPane.showMessageDialog(null,
                                                  "System Failure, Please try again in 15 minutes",
                                                  "System Failure.", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (input == "Student") {
			Iusername = JOptionPane.showInputDialog("Enter Your Username");
			int index = CorrectStudentUsername.indexOf(Iusername);
			int count1 = 1, count2 = 1;
			while (index == -1) {
				Iusername = JOptionPane
                .showInputDialog("Username does not exist!Please Try Again.");
				index = CorrectStudentUsername.indexOf(Iusername);
				count1++;
				if ((count1 >= 3) && (index == -1)) {
					JOptionPane.showMessageDialog(null,
                                                  "System Failure, Please try again in 15 minutes",
                                                  "System Failure.", JOptionPane.ERROR_MESSAGE);
					break;
				}
			}
			if (index != -1) {
				Ipassword = JOptionPane.showInputDialog("Enter Your Password");
				while (count2 < 3) {
					if (Ipassword.equals(CorrectStudentPassword)) {
						JOptionPane.showMessageDialog(null, "Welcome Student.",
                                                      "Welcome", JOptionPane.PLAIN_MESSAGE);
						break;
					} else {
						Ipassword = JOptionPane
                        .showInputDialog("Password incorrect!Please Try Again.");
						count2++;
					}
				}
				if (count2 >= 3) {
					JOptionPane.showMessageDialog(null,
                                                  "System Failure, Please try again in 15 minutes",
                                                  "System Failure.", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (input == "Staff") {
			Iusername = JOptionPane.showInputDialog("Enter Your Username");
			int index = CorrectStaffUsername.indexOf(Iusername);
			int count1 = 1, count2 = 1;
			while (index == -1) {
				Iusername = JOptionPane
                .showInputDialog("Username does not exist!Please Try Again.");
				index = CorrectStaffUsername.indexOf(Iusername);
				count1++;
				if ((count1 >= 3) && (index == -1)) {
					JOptionPane.showMessageDialog(null,
                                                  "System Failure, Please try again in 15 minutes",
                                                  "System Failure.", JOptionPane.ERROR_MESSAGE);
					break;
				}
			}
			if (index != -1) {
				Ipassword = JOptionPane.showInputDialog("Enter Your Password");
				while (count2 < 3) {
					if (Ipassword.equals(CorrectStaffPassword)) {
						JOptionPane.showMessageDialog(null, "Welcome Staff.",
                                                      "Welcome", JOptionPane.PLAIN_MESSAGE);
						break;
					} else {
						Ipassword = JOptionPane
                        .showInputDialog("Password incorrect!Please Try Again.");
						count2++;
					}
				}
				if (count2 >= 3) {
					JOptionPane.showMessageDialog(null,
                                                  "System Failure, Please try again in 15 minutes",
                                                  "System Failure.", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
